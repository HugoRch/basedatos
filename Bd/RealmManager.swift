//
//  RealmManager.swift
//  Bd
//
//  Created by Victor Hugo Reyes on 10/25/19.
//  Copyright © 2019 Víctor Hugo Reyes Chichitz. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {

	private let realm = try! Realm()

	func alta(cliente: Cliente) {
		//Guardar un model
		try! realm.write {
			realm.add(cliente)
		}
		let client = realm.objects(Cliente.self)
		debugPrint(client)
	}

	func editar(nombre: String) {

		let clientes = realm.objects(Cliente.self)

		guard let cliente = clientes.first(where: {$0.nombre == nombre}) else {
			debugPrint("Cliente no encontrado")
			return
		}

		cliente.nombre = "Marco"
		cliente.apellido = "Polo"

		try! realm.write {
			realm.add(cliente)
			debugPrint("Cliente Editado")
		}

	}

	func borrar(nombre: String) {

		let cliente = realm.objects(Cliente.self)
		
		guard let juanClient = cliente.first(where: {$0.nombre == nombre}) else {
			debugPrint("Cliente no encontrado")
			return
		}

		try! realm.write {
			realm.delete(juanClient)
			debugPrint("Cliente eliminado")
		}

	}

	func deleteAll() {
		try! realm.write {
			realm.delete(Array(realm.objects(Cliente.self)))
		}

	}

}

class Cliente: Object {

	@objc dynamic var id: String = NSUUID().uuidString
	@objc dynamic var nombre: String = ""
	@objc dynamic var apellido: String = ""

	// Primary key for Realm database
	override static func primaryKey() -> String? {
		return "id"
	}
}
