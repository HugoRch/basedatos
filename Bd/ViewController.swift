//
//  ViewController.swift
//  Bd
//
//  Created by Víctor Hugo Reyes Chichitz on 10/25/19.
//  Copyright © 2019 Víctor Hugo Reyes Chichitz. All rights reserved.
//

import UIKit
import RealmSwift
import KeychainSwift

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.

        userDefault()



        let realmManager = RealmManager()

		let cliente = Cliente()
		cliente.nombre = "Juan"
		cliente.apellido = "Perez"

		//realmManager.alta(cliente: cliente)

        //realmManager.deleteAll()

	}

    func userDefault() {
        let standard = UserDefaults.standard

        standard.set(true, forKey: "isLogin")

        let isLogin = standard.bool(forKey: "isLogin")

        debugPrint(isLogin)

        //borrar
        //standard.removeObject(forKey: isLogin)

    }

    func keyChain() {

        let keyChain = KeychainSwift()
        keyChain.set("Nombre", forKey: "name_Key")

        let nombre = keyChain.get("name_Key")

        debugPrint(nombre)

        //keyChain.delete(name_Key")

    }

    @IBAction func registrar(_ sender: UIButton) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        present(detailVC, animated: true, completion: nil)
        
    }

}


